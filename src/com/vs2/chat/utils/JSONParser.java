package com.vs2.chat.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;


public class JSONParser {
	
	  static InputStream is = null;
	    static JSONObject jObj = null;
	    static JSONArray jsonArray = null;
	    static String json = "";
	 
	    // constructor
	    public JSONParser() {
	 
	    }
	 
	    public JSONObject getJSONFromUrl(String url){
	    	return getJSONFromUrl(url,null);
	    }
	    
	    public JSONObject getJSONFromUrl(String url,List<NameValuePair> params) {

	        // Making HTTP request
	        try {
	            // defaultHttpClient
				
	            DefaultHttpClient httpClient = new DefaultHttpClient();
	            HttpPost httpPost = new HttpPost(url);
	            
	            // Request parameters and other properties.
	            if(params != null && params.size() > 0){
	            	httpPost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
	            }
	 
	            HttpResponse httpResponse = httpClient.execute(httpPost);
	            HttpEntity httpEntity = httpResponse.getEntity();
	            is = httpEntity.getContent();           
	        } catch (UnsupportedEncodingException e) {
	            e.printStackTrace();
	        } catch (ClientProtocolException e) {
	            e.printStackTrace();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	 
	        try {
	            BufferedReader reader = new BufferedReader(new InputStreamReader(
	                    is, "iso-8859-1"), 8);
	            StringBuilder sb = new StringBuilder();
	            String line = null;
	            while ((line = reader.readLine()) != null) {
	                sb.append(line + "\n");
	            }
	            is.close();
	            json = sb.toString();
	            //Logcat.e("JsonString", "Json " + json);
	        } catch (Exception e) {
	            Logcat.e("Buffer Error", "Error converting result " + e.toString());
	        }
	 
	        // try parse the string to a JSON object
	        try {
	        	//Logcat.e("JsonString", json);
	            jObj = new JSONObject(json);
	        } catch (JSONException e) {
	            Log.e("JSON Parser", "Error parsing data " + e.toString());
	        }
	 
	        // return JSON String
	        return jObj;
	 
	    }
	    
	    public JSONArray getJSONArrayFromUrlPOST(String url) {
	    	return getJSONArrayFromUrl(url, "POST");
	    }
	    
	    public JSONArray getJSONArrayFromUrlGET(String url) {
	    	return getJSONArrayFromUrl(url, "GET");
	    }
	    private JSONArray getJSONArrayFromUrl(String url,String type) {

	        // Making HTTP request
	        try {
	            // defaultHttpClient
	        	//String ua = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.117 Safari/537.36";
	            DefaultHttpClient httpClient = new DefaultHttpClient();
	 
	            //httpPost.setHeader("User-Agent", ua);      
	 
	            HttpResponse httpResponse = httpClient.execute(type.equals("GET")?new HttpGet(url):new HttpPost(url));
	            HttpEntity httpEntity = httpResponse.getEntity();
	           
	            is = httpEntity.getContent();           
	 
	        } catch (UnsupportedEncodingException e) {
	            e.printStackTrace();
	        } catch (ClientProtocolException e) {
	            e.printStackTrace();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	 
	        try {
	            BufferedReader reader = new BufferedReader(new InputStreamReader(
	                    is, "iso-8859-1"), 8);
	            //BufferedReader reader = new BufferedReader(new InputStreamReader(
	             //       is, "UTF-8"), 8);
	            StringBuilder sb = new StringBuilder();
	            String line = null;
	            while ((line = reader.readLine()) != null) {
	                //sb.append(line + "\n");
	            	sb.append(line);
	            }
	            is.close();
	            json = sb.toString();
	            //Logcat.e("Json",json);
	        } catch (Exception e) {
	            Logcat.e("Buffer Error", "Error converting result " + e.toString());
	        }
	 
	        // try parse the string to a JSON object
	        try {
	            jsonArray = new JSONArray(json);
	        } catch (JSONException e) {
	            Logcat.e("JSON Parser", "Error parsing data " + e.toString());
	        }
	 
	        // return JSON String
	        return jsonArray;
	 
	    }
	    
	    public String getStringFromUrl(String url) {

	    	
	        // Making HTTP request
	        try {
	            // defaultHttpClient
				
	            DefaultHttpClient httpClient = new DefaultHttpClient();
	            HttpPost httpPost = new HttpPost(url);
	 
	            HttpResponse httpResponse = httpClient.execute(httpPost);
	            HttpEntity httpEntity = httpResponse.getEntity();
	            is = httpEntity.getContent();           
	        } catch (UnsupportedEncodingException e) {
	            e.printStackTrace();
	        } catch (ClientProtocolException e) {
	            e.printStackTrace();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	 
	        try {
	            BufferedReader reader = new BufferedReader(new InputStreamReader(
	                    is, "iso-8859-1"), 8);
	            StringBuilder sb = new StringBuilder();
	            String line = null;
	            while ((line = reader.readLine()) != null) {
	                sb.append(line + "\n");
	            }
	            
	            //Logcat.e("Get PinCode JsonParser",""+sb.toString());	            
	            is.close();
	            return sb.toString();
	            
	        } catch (Exception e) {
	            //Log.e("Buffer Error", "Error converting result " + e.toString());
	        }
	        return null;
	    }


}
