package com.vs2.chat.utils;

import java.math.BigDecimal;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.net.ConnectivityManager;
import android.util.Base64;
import android.widget.EditText;

public class Utility {

	/**
	 * 
	 * @param edit
	 *            : EditText to validate
	 * @param validateEmail
	 *            : if true validate email also
	 * @return true/false
	 */
	public static boolean notBlank(EditText edit, boolean validateEmail) {
		if (edit.getText().length() > 0) {
			if (validateEmail) {
				if (!Utility.isValidEmail(edit.getText().toString())) {
					edit.setError("Not valid email!");
					return false;
				}
			}
			edit.setError(null);
			return true;

		} else {
			edit.setError("Required " + edit.getHint());
			return false;
		}
	}

	/**
	 * 
	 * @param target
	 *            : send email to validate
	 * @return true/false
	 */
	public static boolean isValidEmail(CharSequence target) {
		return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
	}

	/**
	 * 
	 * @param context
	 * @return true/false Check Internet connection available
	 */
	public static boolean isOnline(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		return (cm == null || cm.getActiveNetworkInfo() == null) ? false : cm
				.getActiveNetworkInfo().isConnectedOrConnecting();
	}

	
	public static void getHashKey(Context context) {
		try {
			PackageInfo info = context.getPackageManager().getPackageInfo(
					"com.vs2.chat", PackageManager.GET_SIGNATURES);
			for (Signature signature : info.signatures) {
				MessageDigest md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				Logcat.e("KeyHash:",
						Base64.encodeToString(md.digest(), Base64.DEFAULT));
			}
		} catch (NameNotFoundException e) {

		} catch (NoSuchAlgorithmException e) {

		}
	}
	
	/**
     * Round to certain number of decimals
     * 
     * @param d
     * @param decimalPlace
     * @return
     */
    public static float round(float d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.floatValue();
    }

}
