package com.vs2.chat;


import java.io.File;
import java.util.Collection;

import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.MessageTypeFilter;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.Presence.Mode;
import org.jivesoftware.smack.provider.ProviderManager;
import org.jivesoftware.smack.util.StringUtils;
import org.jivesoftware.smackx.MessageEventManager;
import org.jivesoftware.smackx.MessageEventNotificationListener;
import org.jivesoftware.smackx.MessageEventRequestListener;
import org.jivesoftware.smackx.bytestreams.socks5.provider.BytestreamsProvider;
import org.jivesoftware.smackx.filetransfer.FileTransfer.Status;
import org.jivesoftware.smackx.filetransfer.FileTransferListener;
import org.jivesoftware.smackx.filetransfer.FileTransferManager;
import org.jivesoftware.smackx.filetransfer.FileTransferRequest;
import org.jivesoftware.smackx.filetransfer.IncomingFileTransfer;
import org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer;
import org.jivesoftware.smackx.provider.DiscoverInfoProvider;
import org.jivesoftware.smackx.provider.DiscoverItemsProvider;
import org.jivesoftware.smackx.receipts.DeliveryReceipt;
import org.jivesoftware.smackx.receipts.DeliveryReceiptManager;
import org.jivesoftware.smackx.receipts.DeliveryReceiptRequest;
import org.jivesoftware.smackx.receipts.ReceiptReceivedListener;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.vs2.chat.utils.Logcat;
import com.vs2.chat.utils.Utility;

public class SignUpActivity extends Activity{

	
	public static final int PORT = 5222;
	/*
	public static final String HOST = "202.179.83.48";
	public static final String SERVICE = "202.179.83.48";
	*/
	
	public static final String HOST = "74.50.103.214";
	public static final String SERVICE = "74.50.103.214";
	
	public static final String TAG = "VS2Chat";
	
	private EditText mEditUserName,mEditName,mEditEmail,mEditPassword;
	private Button mButtonJoin;
	
	Roster roster;
	/**
	 * Connection Declaration
	 */
	XMPPConnection connection;
	ConnectionConfiguration connConfig;
	Chat chat;
	
	MessageEventManager eventManger;
	
	FileTransferManager manager; 
	
	
	public static final String SENDER = "246"; //k = 203 & d=246
	public static final String RECEIEVER = "203";
	
	public static final String PASSWORD = "ddddd";
	
	
	public static final String HOSTNAME = "server-mau0";
	
	
	//static final DiscoverInfo.Identity YAXIM_IDENTITY = new DiscoverInfo.Identity("client","antarix","phone");
	
	static{
		//registerSmackProviders();
		ProviderManager.getInstance().addExtensionProvider(DeliveryReceipt.ELEMENT, DeliveryReceipt.NAMESPACE, new DeliveryReceipt.Provider());
		ProviderManager.getInstance().addExtensionProvider(DeliveryReceiptRequest.ELEMENT, new DeliveryReceiptRequest().getNamespace(), new DeliveryReceiptRequest.Provider());
		ProviderManager.getInstance().addIQProvider("query","http://jabber.org/protocol/bytestreams", new BytestreamsProvider());
		ProviderManager.getInstance().addIQProvider("query","http://jabber.org/protocol/disco#items", new DiscoverItemsProvider());
		ProviderManager.getInstance().addIQProvider("query","http://jabber.org/protocol/disco#info", new DiscoverInfoProvider());

	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_signup);
		initGlobals();

		if(Utility.isOnline(SignUpActivity.this)){
			new ConnectChat().execute();
		}else{
			Toast toast = Toast.makeText(SignUpActivity.this, "Please connect to internet", Toast.LENGTH_LONG);
			toast.show();
		}
		
		
	}
	
	private void transferFile(){
		//FileTransferManager manager = new FileTransferManager(connection);
		OutgoingFileTransfer transfer = manager.createOutgoingFileTransfer( RECEIEVER + "@" + HOST +"/Smack");
		//OutgoingFileTransfer transfer = manager.createOutgoingFileTransfer( RECEIEVER + "@" + HOST);
		File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/test.jpg");
		if(!file.exists()){
			Logcat.e(TAG, "File not exists");
			return;
		}
		try {
			
		   transfer.sendFile(file,"this is a test file");
		} catch (XMPPException e) {
		   Logcat.e(TAG, "XMPPException : " + e.toString());
		}
		while(!transfer.isDone()) {
		   if(transfer.getStatus().equals(Status.error)) {
		      Logcat.e(TAG, "ERROR!!! " + transfer.getError());
		   } else if (transfer.getStatus().equals(Status.cancelled)
		                    || transfer.getStatus().equals(Status.refused)) {
			   Logcat.e(TAG, "Cancelled!!! " + transfer.getError());
		   }
		   try {
		      Thread.sleep(1000L);
		   } catch (InterruptedException e) {
		      //e.printStackTrace();
			   Logcat.e(TAG,"InterruptedException" + e.toString());
		   }catch(Exception e){
			   Logcat.e(TAG,"Exception" + e.toString());
		   }
		}
		if(transfer.getStatus().equals(Status.refused)){
			Logcat.e(TAG,"refused error " + transfer.getError());
		}else if(transfer.getStatus().equals(Status.error)){
			Logcat.e(TAG,"main error " + transfer.getError());
		}else if(transfer.getStatus().equals(Status.cancelled)){
		   Logcat.e(TAG,"rcancelled error " + transfer.getError());
		} else {
			Logcat.e(TAG,"Success!!");
		}
	}
	
	private void receieveFile(){
		Logcat.e("File Receieve","Listener set");
		//FileTransferManager manager = new FileTransferManager(connection);
		manager.addFileTransferListener(new FileTransferListener() {
		   public void fileTransferRequest(final FileTransferRequest request) {
			   
			   Logcat.e("File Receieve","In comming file...");
		      new Thread(){
		         @Override
		         public void run() {
		            IncomingFileTransfer transfer = request.accept();
		            //File mf = Environment.getExternalStorageDirectory();
		            //File file = new File(mf.getAbsoluteFile()+"/DCIM/Camera/" + transfer.getFileName());
		            File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + 
		            		transfer.getFileName());
		            try{
		                transfer.recieveFile(file);
		                while(!transfer.isDone()) {
		                   try{
		                      Thread.sleep(1000L);
		                   }catch (Exception e) {
		                      Logcat.e("", e.getMessage());
		                   }
		                   if(transfer.getStatus().equals(Status.error)) {
		                      Logcat.e("ERROR!!! ", transfer.getError() + "");
		                   }
		                   if(transfer.getException() != null) {
		                      transfer.getException().printStackTrace();
		                   }
		                }
		                
		                SignUpActivity.this.runOnUiThread(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								Toast.makeText(SignUpActivity.this,"File receieved successfully!!",
										Toast.LENGTH_LONG).show();
								Logcat.e(TAG, "File receieved successfully!!");
							}
						});
		             }catch (Exception e) {
		                Logcat.e(TAG, e.getMessage());
		            }
		         };
		       }.start();
		    }
		 });
	}
	private void setDelieveryListener(){
		DeliveryReceiptManager.getInstanceFor(connection).enableAutoReceipts();
		DeliveryReceiptManager.getInstanceFor(connection).addReceiptReceivedListener(new ReceiptReceivedListener()
		{
		        @Override
		        public void onReceiptReceived(String fromJid, String toJid, String receiptId)
		        {
		            Logcat.e("app", fromJid + ", " + toJid + ", " + receiptId);
		        }
		        
		});
	}

	
	
	private void initializeConnection(){
		try {
			connConfig = new ConnectionConfiguration(HOST, PORT, SERVICE);
		
			connection = new XMPPConnection(connConfig);
			//connConfig.setSelfSignedCertificateEnabled(true);
			
			//connConfig.setSASLAuthenticationEnabled(true);
			try {
			  //Connect to the server
			  connection.connect();
			  manager = new FileTransferManager(connection);
			} catch (XMPPException ex) {
			  connection = null;
			  Logcat.e("OnConnect", "XMPPException : " + ex.toString());
			  //Unable to connect to server
			}
			
			
			
			try {
				PacketFilter filter = new MessageTypeFilter(Message.Type.chat);
				connection.addPacketListener(new PacketListener() {
					
					@Override
					public void processPacket(Packet packet) {
						// TODO Auto-generated method stub
						Message message = (Message) packet;
						 if (message.getBody() != null) {
					            String fromName = StringUtils.parseBareAddress(message.getFrom());
					            final String msg = " Text Recieved " + message.getBody() + " from " +  fromName;
					            
					            Packet received = new Message();
					            received.addExtension(new DeliveryReceipt(packet.getPacketID()));
					            received.setTo(packet.getFrom());
					            connection.sendPacket(received);
					           
					            Logcat.e("XMPPChatDemoActivity ", msg);
					           eventManger.sendDeliveredNotification(fromName, message.getPacketID());
					            SignUpActivity.this.runOnUiThread(new Runnable() {
									
									@Override
									public void run() {
										// TODO Auto-generated method stub
										Toast.makeText(SignUpActivity.this, msg,Toast.LENGTH_SHORT).show();
									}
								});
					           
					     }else{
					    	 Logcat.e("XMPPChatDemoActivity ", "Message is null");
					     }
						    
					}
				}, filter);
				connection.login( SENDER+"@" + SERVICE,PASSWORD);
				//connection.login("mitesh@antarix","welcome");
				//connection.login("a@server-mau0","aaaaa");
				// Create a new presence. Pass in false to indicate we're unavailable.
				Presence presence = new Presence(Presence.Type.available);
				presence.setStatus("I�m available");
				connection.sendPacket(presence);
				
				
					getRooster();
		            
		            
		            roster.addRosterListener(new RosterListener() {
		            	 @Override
		            	 public void presenceChanged(Presence presence) {
		            	   //Called when the presence of a roster entry is changed
		            		 Logcat.e("Rooster listner","Persence changed");
		            		 getRooster();
		            	 }
		            	 @Override
		            	 public void entriesUpdated(Collection<String> arg0) {
		            	   // Called when a roster entries are updated.
		            		 Logcat.e("Rooster listner","entries updated");
		            		 getRooster();
		            	 }
		            	 @Override
		            	 public void entriesDeleted(Collection<String> arg0) {
		            	   // Called when a roster entries are removed.
		            		 Logcat.e("Rooster listner","entries deleted");
		            		 getRooster();
		            	 }
		            	@Override
		            	 public void entriesAdded(Collection<String> arg0) {
		            	   // Called when a roster entries are added.
		            		Logcat.e("Rooster listner","entries added");
		            		getRooster();
		            		
		            	 }
		            	});  
			
		            
				
			} catch (Exception e) {
				// TODO: handle exception
				Logcat.e("Login", "Error login : " + e.toString());
			}
			
			
		} catch (Exception e) {
			// TODO: handle exception
			Logcat.e("OnConnect", "Error in connection : " + e.toString());
		}
	}
	
	
	private void setEventListeners(){
		
		eventManger = new MessageEventManager(connection);
        eventManger.addMessageEventNotificationListener(new MessageEventNotificationListener() {
			
			@Override
			public void offlineNotification(String arg0, String arg1) {
				// TODO Auto-generated method stub
				Logcat.e("MessageEvent", "Offline notification");
			}
			
			@Override
			public void displayedNotification(String arg0, String arg1) {
				// TODO Auto-generated method stub
				Logcat.e("MessageEvent", "displayed notification");
			}
			
			@Override
			public void deliveredNotification(String arg0, String arg1) {
				// TODO Auto-generated method stub
				Logcat.e("MessageEvent", "delievered notification");
			}
			
			@Override
			public void composingNotification(String arg0, String arg1) {
				// TODO Auto-generated method stub
				Logcat.e("MessageEvent", "composing notification");
			}
			
			@Override
			public void cancelledNotification(String arg0, String arg1) {
				// TODO Auto-generated method stub
				Logcat.e("MessageEvent", "cancelled notification");
			}
		});
        eventManger.addMessageEventRequestListener(new MessageEventRequestListener() {
			
			@Override
			public void offlineNotificationRequested(String arg0, String arg1,
					MessageEventManager arg2) {
				// TODO Auto-generated method stub
				Logcat.e("MessageEvent", "Offline");
			}
			
			@Override
			public void displayedNotificationRequested(String arg0, String arg1,
					MessageEventManager arg2) {
				// TODO Auto-generated method stub
				Logcat.e("MessageEvent", "Displayed");
			}
			
			@Override
			public void deliveredNotificationRequested(String arg0, String arg1,
					MessageEventManager arg2) {
				// TODO Auto-generated method stub
				Logcat.e("MessageEvent", "Delievered.");
				 eventManger.sendDeliveredNotification(arg0, arg1);
			}
			
			@Override
			public void composingNotificationRequested(String arg0, String arg1,
					MessageEventManager arg2) {
				// TODO Auto-generated method stub
				Logcat.e("MessageEvent", "Typing....");
			}
		});
	}
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		 // Disconnect from the server
		
		super.onDestroy();
	}
	
	public void getRooster(){
		//Get friends list
		 roster = connection.getRoster();
		 /*
           Collection<RosterEntry> entries = roster.getEntries();
           for (RosterEntry entry : entries) {

           	Logcat.e("XMPPChatDemoActivity",  "--------------------------------------");
             Logcat.e("XMPPChatDemoActivity", "RosterEntry " + entry);
             Logcat.e("XMPPChatDemoActivity", "User: " + entry.getUser());
             Logcat.e("XMPPChatDemoActivity", "Name: " + entry.getName());
             Logcat.e("XMPPChatDemoActivity", "Status: " + entry.getStatus());
             Logcat.e("XMPPChatDemoActivity", "Type: " + entry.getType());
             Presence entryPresence = roster.getPresence(entry.getUser());

             Logcat.e("XMPPChatDemoActivity", "Presence Status: "+ entryPresence.getStatus());
             Logcat.e("XMPPChatDemoActivity", "Presence Type: " + entryPresence.getType());

             Presence.Type type = entryPresence.getType();
             if (type == Presence.Type.available){
           	  Logcat.e("XMPPChatDemoActivity", "Presence AVIALABLE");  
             }else{
           	  Logcat.e("XMPPChatDemoActivity", "Presence : " + entryPresence);
             }
              
               
         }
         
         */
	}
	
	private void initGlobals(){
		mEditUserName = (EditText)findViewById(R.id.edit_username);
		mEditName = (EditText)findViewById(R.id.edit_name);
		mEditEmail = (EditText)findViewById(R.id.edit_email);
		mEditPassword = (EditText)findViewById(R.id.edit_password);
		
		mButtonJoin = (Button)findViewById(R.id.button_join);
		mButtonJoin.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//if (Utility.notBlank(mEditUserName, false)
					//	&& Utility.notBlank(mEditPassword, false)){
					
					
					if(!Utility.isOnline(SignUpActivity.this)){
						toast("No internet connection found!",true);
						return;
					}
					//new LoginApi().execute(mEditUserName.getText().toString(), mEditPassword.getText().toString());
					
					//new LoginApi().execute(SENDER + "@" + SERVICE,PASSWORD);
					new SendFile().execute();	
					
					
				//}
						
				
				boolean flag = true;
				if(flag){
					return;
				}
				if (Utility.notBlank(mEditUserName, false)
						&& Utility.notBlank(mEditName,false)
						&& Utility.notBlank(mEditEmail,true)
						&& Utility.notBlank(mEditPassword,false)) {
					
					//Check Internet connection is available
					if(!Utility.isOnline(SignUpActivity.this)){
						toast("No internet connection found!",true);
						return;
					}
					
					
					/*
					
					// Create the configuration for this new connection
					ConnectionConfiguration config = new ConnectionConfiguration("jabber.org", 5222);
					config.setCompressionEnabled(true);
			
					Connection connection = new XMPPConnection(config);
					// Connect to the server
					connection.connect();
					// Log into the server
					connection.login("username", "password", "SomeResource");
					
					// Disconnect from the server
					connection.disconnect();
					
					*/
					
				}
			}
		});
	}
	
	private void toast(String message, boolean longToast) {
		if (message.length() > 0) {
			Toast.makeText(SignUpActivity.this, message,
					longToast ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT).show();
		}
	}
	
	public class LoginApi extends AsyncTask<String, Integer, Boolean>{

		ProgressDialog pd;
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			pd = new ProgressDialog(SignUpActivity.this);
			pd.setMessage("Logging...");
			pd.setCancelable(false);
			pd.setCanceledOnTouchOutside(false);
			pd.show();
			super.onPreExecute();
		}
		
		@Override
		protected Boolean doInBackground(String... params) {
			// TODO Auto-generated method stub
			try {
				
					Message msg = new Message( RECEIEVER+"@" + SERVICE, Message.Type.chat);
					//Message msg = new Message("mitesh@server-mau0", Message.Type.chat);
					//MessageEventManager.addNotificationsRequests(msg, true, true, true, true);
					chat = connection.getChatManager().createChat( RECEIEVER+"@"+ SERVICE,null);
					
					msg.setBody("This is a new message");
					DeliveryReceiptManager.addDeliveryReceiptRequest(msg);
					Logcat.e("Sent message", "Packet Id " + msg.getPacketID());
					MessageEventManager.addNotificationsRequests(msg, true, true, true, true);
					
					if (connection != null) {
						//connection.sendPacket(msg);
						chat.sendMessage(msg);
						Thread.sleep(300);
					}
				
				return true;
			} catch (Exception e) {
				// TODO: handle exception
				Logcat.e("Login", "Error connecting.. " + e.toString());
				return false;
			}
			
		}
		
		@Override
		protected void onPostExecute(Boolean result) {
			// TODO Auto-generated method stub
			try {
				pd.dismiss();
				if(result){
					Toast.makeText(SignUpActivity.this, "Login successful", Toast.LENGTH_LONG).show();
				}else{
					Toast.makeText(SignUpActivity.this, "Login failed", Toast.LENGTH_LONG).show();
				}
				
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			super.onPostExecute(result);
		}
	}
	
	public class SendFile extends AsyncTask<String, Integer, Boolean>{

		ProgressDialog pd;
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			pd = new ProgressDialog(SignUpActivity.this);
			pd.setMessage("Sending file...");
			pd.setCancelable(false);
			pd.setCanceledOnTouchOutside(false);
			pd.show();
			super.onPreExecute();
		}
		
		@Override
		protected Boolean doInBackground(String... params) {
			// TODO Auto-generated method stub
			try {
				
				transferFile();	
				return true;
			} catch (Exception e) {
				// TODO: handle exception
				Logcat.e("Send file", "Error sending.. " + e.toString());
				return false;
			}
			
		}
		
		@Override
		protected void onPostExecute(Boolean result) {
			// TODO Auto-generated method stub
			try {
				pd.dismiss();
				if(result){
					Toast.makeText(SignUpActivity.this, "File sent successful", Toast.LENGTH_LONG).show();
				}else{
					Toast.makeText(SignUpActivity.this, "File sending failed", Toast.LENGTH_LONG).show();
				}
				
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			super.onPostExecute(result);
		}
	}

	public static int getPresenceMode(Mode userMode, boolean isOnline) {
	        int userState = 0;
	        /** 0 for offline, 1 for online, 2 for away,3 for busy*/
	        if(userMode == Mode.dnd) {
	            userState = 3;
	            Logcat.e("Presence", "DND");
	        } else if (userMode == Mode.away || userMode == Mode.xa) {   
	            userState = 2;
	            Logcat.e("Presence", "Away");
	        } else if (isOnline) {
	            userState = 1;
	            Logcat.e("Presence", "Online");
	        }else{
	        	Logcat.e("Presence", "Offline");
	        }
	        return userState;
	}
	
	public class ConnectChat extends AsyncTask<String, Integer, Boolean>{

		ProgressDialog pd;
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			pd = new ProgressDialog(SignUpActivity.this);
			pd.setMessage("Connecting...");
			pd.setCancelable(false);
			pd.setCanceledOnTouchOutside(false);
			pd.show();
			super.onPreExecute();
		}
		
		@Override
		protected Boolean doInBackground(String... params) {
			// TODO Auto-generated method stub
			try {
				initializeConnection();
				return true;
			} catch (Exception e) {
				// TODO: handle exception
				Logcat.e("Connect", "Error connecting.. " + e.toString());
				return false;
			}
			
		}
		
		@Override
		protected void onPostExecute(Boolean result) {
			// TODO Auto-generated method stub
			try {
				pd.dismiss();
				if(result){
					Toast.makeText(SignUpActivity.this, "Connection successful", Toast.LENGTH_LONG).show();
					receieveFile();
					setDelieveryListener();
					Presence availability = roster.getPresence( RECEIEVER +"@" + SERVICE);
					Mode userMode = availability.getMode();

					getPresenceMode(userMode,availability.isAvailable());
					
				}else{
					Toast.makeText(SignUpActivity.this, "Unable to connect", Toast.LENGTH_LONG).show();
				}
				
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			super.onPostExecute(result);
		}
	}

	

}
